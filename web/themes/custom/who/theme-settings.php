<?php

use \Drupal\Core\Form\FormStateInterface;

function who_form_system_theme_settings_alter (&$form, FormStateInterface &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['who_copyright_message'] = [
    '#type' => 'textfield',
    '#title' => t('Copyright message'),
    '#default_value' => theme_get_setting('who_copyright_message'),
  ];
}